﻿using GTA;
using GTA.Native;
using System;
using System.Windows.Forms;

namespace ToggleStuntJumps
{
    public class ToggleStuntJumps : Script
    {
        bool enabled;
        Keys toggleKey;

        public ToggleStuntJumps()
        {
            enabled = Settings.GetValue("SETTINGS", "JUMPS_ENABLED", false);
            toggleKey = Settings.GetValue("SETTINGS", "TOGGLE_KEY", Keys.PageUp);

            if (toggleKey != Keys.None) KeyUp += ToggleStuntJumps_KeyUp;
            Tick += ToggleStuntJumps_Tick;
        }

        private void ToggleStuntJumps_Tick(object sender, EventArgs e)
        {
            if (Game.IsLoading || Game.IsScreenFadedOut || Game.IsScreenFadingIn) return;

            SetStuntJumps(enabled);

            Tick -= ToggleStuntJumps_Tick;
        }

        private void ToggleStuntJumps_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == toggleKey)
            {
                enabled = !enabled;
                SetStuntJumps(enabled);

                UI.ShowSubtitle((enabled ? "Enabled" : "Disabled") + " stunt jumps");
            }
        }

        private void SetStuntJumps(bool enabled)
        {
            Hash nativeHash = (enabled ? Hash.ENABLE_STUNT_JUMP_SET : Hash.DISABLE_STUNT_JUMP_SET);

            Function.Call(nativeHash, 0);
            Function.Call(nativeHash, 1);
            Function.Call(nativeHash, 2);
        }
    }
}
